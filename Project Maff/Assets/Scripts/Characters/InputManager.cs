﻿using UnityEngine;
using System.Collections;

public class KeyInfo
{
    string keyName;

    public bool pressedThisFrame; //This bool is true for one frame if the key is pressed down
    public bool doubleTapThisFrame; //true if key is pressed twice in succcession. Overrides pressedThisFrame

    public bool isHeld; // true as long as the button is held
    public float timeSinceLastPress; //time since the last press in seconds
    public float doubleTapTime = 0.15f;

    public KeyInfo(string name)
    {
        keyName = name;
    }

    public void isPressed()
    {
        //Check doubletap
        if (timeSinceLastPress < doubleTapTime )
        {
            timeSinceLastPress = 0f;
            pressedThisFrame = true;
            doubleTapThisFrame = true;
            isHeld = true;

            Debug.Log("Doubletapped " + keyName);
        }
        else
        {
            timeSinceLastPress = 0f;
            pressedThisFrame = true;
            isHeld = true;

            Debug.Log("pressed " + keyName);
        }
    }

    public void isReleased()
    {
        isHeld = false;
        Debug.Log("released " + keyName);
    }

    public void Update(float deltaTime)
    {
        timeSinceLastPress += deltaTime;
        pressedThisFrame = false;
    }


}

public class InputManager : MonoBehaviour {

    public float directionalInputDeadzone = 0.15f;

    //This script should be used to check for input meant for the player character. It is then sent to the statemanager for proccessing.

    public Vector2 directionalInputVector; //A vector of the directional keys
    public Vector2 lastFrameDirectionalInputVector = Vector2.zero;
    public bool useInputVector = false;

    public KeyInfo keyForward = new KeyInfo("Forward");
    public KeyInfo keyBackward = new KeyInfo("Backward");
    public KeyInfo keyLeft = new KeyInfo("Left");
    public KeyInfo keyRight = new KeyInfo("Right");
    public KeyInfo keyJump = new KeyInfo("Jump");
    public KeyInfo keyRoll = new KeyInfo("Roll");
    public KeyInfo keyPrimary = new KeyInfo("Primary Attack");
    public KeyInfo keySecondary = new KeyInfo("Secondary Attack");
    public KeyInfo keyAbillity1 = new KeyInfo("Abillity 1");
    public KeyInfo keyAbillity2 = new KeyInfo("Abillity 2");
    public KeyInfo keyAbillity3 = new KeyInfo("Abillity 3");
    public KeyInfo keyAbillity4 = new KeyInfo("Abillity 4");
    public KeyInfo keyItem1 = new KeyInfo("Item 1");
    public KeyInfo keyItem2 = new KeyInfo("Item 2");



    public CharacterMotor characterMotor;

	// Update is called once per frame
	void Update () {
        UpdateInput();


        //

        characterMotor.AddMovement( new Vector3(directionalInputVector.x, 0, directionalInputVector.y), 1000f, 10.2f);
        //
	}

    void UpdateInput()
    {
        UpdateKeyInfo(); //Update KeyInfo first, since it resets variables.

        UpdateDirectionalInput(); //Simulates keypresses with axis
        UpdateButtonInput(); //Checks for all keypresses on buttons

        
    }

    void UpdateDirectionalInput()
    {
        //Update Inputvector
        directionalInputVector = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        
        //Update KeyInfo based on the input vector
        //If the input vector is moved over the deadzone, and was under it last frame, then it's been "pressed"

        //Forward
        //Check if pressed
        if (InputInNeutralPosition(lastFrameDirectionalInputVector) && directionalInputVector.y > directionalInputDeadzone )
        {
            keyForward.isPressed();
        }
        //Check if released
        else if(InputInNeutralPosition(directionalInputVector) && lastFrameDirectionalInputVector.y > directionalInputDeadzone)
        {
            keyForward.isReleased();
        }

        //Back
        //Check if pressed
        if (InputInNeutralPosition(lastFrameDirectionalInputVector) && directionalInputVector.y < (directionalInputDeadzone*-1))
        {
            keyBackward.isPressed();
        }
        //Check if released
        else if (InputInNeutralPosition(directionalInputVector) && lastFrameDirectionalInputVector.y < (directionalInputDeadzone*-1))
        {
            keyBackward.isReleased();
        }

        //Right
        //Check if pressed
        if (InputInNeutralPosition(lastFrameDirectionalInputVector) && directionalInputVector.x > directionalInputDeadzone)
        {
            keyRight.isPressed();
        }
        //Check if released
        else if (InputInNeutralPosition(directionalInputVector) && lastFrameDirectionalInputVector.x > directionalInputDeadzone)
        {
            keyRight.isReleased();
        }

        //Left
        //Check if pressed
        if (InputInNeutralPosition(lastFrameDirectionalInputVector) && directionalInputVector.x < (directionalInputDeadzone * -1))
        {
            keyLeft.isPressed();
        }
        //Check if released
        else if (InputInNeutralPosition(directionalInputVector) && lastFrameDirectionalInputVector.x < (directionalInputDeadzone * -1))
        {
            keyLeft.isReleased();
        }


        lastFrameDirectionalInputVector = directionalInputVector;
    }

    void UpdateButtonInput()
    {

        //Primary Attack Pressed
        if(Input.GetButtonDown("Primary Attack"))
        {
            keyPrimary.isPressed();
        }
        //Primary Attack Releaseed
        if (Input.GetButtonUp("Primary Attack"))
        {
            keyPrimary.isReleased();
        }

        //Secondary Attack Pressed
        if (Input.GetButtonDown("Secondary Attack"))
        {
            keySecondary.isPressed();
        }
        //Secondary Attack Releaseed
        if (Input.GetButtonUp("Secondary Attack"))
        {
            keySecondary.isReleased();
        }

        //Jump Pressed
        if (Input.GetButtonDown("Jump"))
        {
            keyJump.isPressed();
        }
        //Jump Releaseed
        if (Input.GetButtonUp("Jump"))
        {
            keyJump.isReleased();
        }

        //Abillity 1 Pressed
        if (Input.GetButtonDown("Abillity 1"))
        {
            keyAbillity1.isPressed();
        }
        //Abillity 1 Releaseed
        if (Input.GetButtonUp("Abillity 1"))
        {
            keyAbillity1.isReleased();
        }

        //Abillity 2 Pressed
        if (Input.GetButtonDown("Abillity 2"))
        {
            keyAbillity2.isPressed();
        }
        //Abillity 2 Releaseed
        if (Input.GetButtonUp("Abillity 2"))
        {
            keyAbillity2.isReleased();
        }

        //Abillity 3 Pressed
        if (Input.GetButtonDown("Abillity 3"))
        {
            keyAbillity3.isPressed();
        }
        //Abillity 3 Releaseed
        if (Input.GetButtonUp("Abillity 3"))
        {
            keyAbillity3.isReleased();
        }

        //Abillity 4 Pressed
        if (Input.GetButtonDown("Abillity 4"))
        {
            keyAbillity4.isPressed();
        }
        //Abillity 4 Releaseed
        if (Input.GetButtonUp("Abillity 4"))
        {
            keyAbillity4.isReleased();
        }

        //keyItem 1 Pressed
        if (Input.GetButtonDown("Item 1"))
        {
            keyItem1.isPressed();
        }
        //keyItem 1 Releaseed
        if (Input.GetButtonUp("Item 1"))
        {
            keyItem1.isReleased();
        }

        //keyItem 2 Pressed
        if (Input.GetButtonDown("Item 2"))
        {
            keyItem2.isPressed();
        }
        //keyItem 2 Releaseed
        if (Input.GetButtonUp("Item 2"))
        {
            keyItem2.isReleased();
        }

    }

    void UpdateKeyInfo()
    {
        keyForward.Update(Time.deltaTime);
        keyBackward.Update(Time.deltaTime);
        keyLeft.Update(Time.deltaTime);
        keyRight.Update(Time.deltaTime);

        keyJump.Update(Time.deltaTime);
        keyRoll.Update(Time.deltaTime);

        keyPrimary.Update(Time.deltaTime);
        keySecondary.Update(Time.deltaTime);

        keyAbillity1.Update(Time.deltaTime);
        keyAbillity2.Update(Time.deltaTime);
        keyAbillity3.Update(Time.deltaTime);
        keyAbillity4.Update(Time.deltaTime);

        keyItem1.Update(Time.deltaTime);
        keyItem2.Update(Time.deltaTime);

    }

    bool InputInNeutralPosition(Vector2 input)
    {
        //horizontal
        if((input.x > (directionalInputDeadzone*-1)) && (input.x < (directionalInputDeadzone)))
        {
            //Vertical
            if ((input.y > (directionalInputDeadzone * -1)) && (input.y < (directionalInputDeadzone)))
            {
                return true;

            }
        }
        return false;
    }
}
