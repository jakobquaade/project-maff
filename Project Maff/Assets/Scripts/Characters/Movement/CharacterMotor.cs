﻿using UnityEngine;
using System.Collections;

public class CharacterMotor : MonoBehaviour {

    public float gravity = -9.89f; //Gravity.

    public float friction = 0.1f; //How much velocity the player should lose over time.

    public float maxMovementSpeed = 10f; //How fast the player may move with playerMovement. units per second
    public float absoluteMaxSpeed = 100f; //The final movement velocity is clamped to this value.

    public float rotationSpeed = 1f; //Degrees per second

    public bool grounded = false;

    public bool useSliding =false;
    public float slideAngleLimit;

    public float antiBumpValue = -0.5f; //How much we should be moving downwards when we're grounded to avoid bumping down slopes

    Vector3 playerMovement;
    Vector3 forceMovement;
    Vector3 currentMovement;


    CharacterController characterController;
    Vector3 lastPosition;
    PhotonView photonView;
    PhotonTransformView transformView;

    private Vector3 contactPoint;
    private RaycastHit hit;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
        photonView = GetComponent<PhotonView>();
        transformView = GetComponent<PhotonTransformView>();
    }

    void Update()
    {
        grounded = characterController.isGrounded; //Update our grounded variable. We just use the character controllers.

        CheckSliding();
        UpdateMovement(); 
        

    }

    void UpdateMovement()
    {
        currentMovement = playerMovement + forceMovement; //Add together the movement vectors
        if (!grounded) { currentMovement += Vector3.up * gravity; } //Add gravity if we aren't grounded
        else
        { currentMovement += Vector3.down * antiBumpValue; }
        currentMovement *= friction * Time.deltaTime;

        currentMovement = transform.TransformDirection(currentMovement);

        characterController.Move(currentMovement);

        Debug.Log(currentMovement.magnitude);
    }


    /// <summary>
    /// Checks if we are on a surface that is too steep to stand on. If we are, we set sliding to true (not used atm)
    /// </summary>
    void CheckSliding()
    {
        if (grounded)
        {
            if (useSliding)
            {
                bool sliding = false;

                if (Physics.Raycast(contactPoint + Vector3.up, -Vector3.up, out hit)) //Raycast below the controller to get the angle of the surface we hit.
                {
                    if (Vector3.Angle(hit.normal, Vector3.up) > slideAngleLimit) //If it's higher than the max angle, set sliding to true.
                    {
                        sliding = true;
                        grounded = false;
                    }
                }
                else //Raycasting straight down on steep slopes can fail, so we raycast from the hit point instead.
                {
                    Physics.Raycast(contactPoint + Vector3.up, -Vector3.up, out hit);
                    if (Vector3.Angle(hit.normal, Vector3.up) > slideAngleLimit)
                    {
                        sliding = true;
                        grounded = false;
                    }
                }
            }
        }

    }

    /// <summary>
    /// Adds force to the player in the specified direction, with acceleration as the force. The total velocity of the player, 
    /// with every other movement taken in account cannot be faster than max speed. The player force is clamped to stay within the max speed.
    /// </summary>
    /// <param name="direction">The direction </param>
    /// <param name="acceleration">the force</param>
    /// <param name="maxSpeed">maximum length of the players movement vector.</param>
    public void AddMovement(Vector3 direction, float acceleration, float maxSpeed) 
    {
        playerMovement += direction.normalized * acceleration * Time.deltaTime;
        playerMovement = Vector3.ClampMagnitude(playerMovement, maxSpeed);  
    }

    /// <summary>
    /// Adds force to the player in the direction. this force is seperate from the movement force, and can exceed max movement.
    /// </summary>
    /// <param name="direction"></param>
    /// <param name="force"></param>
    public void AddForce(Vector3 direction, float force) 
    {
        forceMovement += direction.normalized * force;
    }

    public void SetForce(Vector3 direction, float force)
    {
        forceMovement = direction.normalized * force;
    }

    public void RotateTowardsCamera(float rotationSpeed, Transform cameraRotation)
    {

    }

    



}
